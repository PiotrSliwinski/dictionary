#include "dictionary.h"
#include <iostream>
using namespace std;

void Dictionary::load()
{
file.open( "slownik.txt", ios::in | ios::binary);
if( !file.good() )
    cout << "Failed to load dictionary!" << endl;
}

void Dictionary::download_words()
{
while( !file.eof() )
    {
        file >> data;
        dictionary_.push_back(data);
    }
}

bool Dictionary::compare(const string & text)
{
for( auto it : dictionary_ )
    {
       return (it == text);
    }
}

