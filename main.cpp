#include "input_text.h"
#include "dictionary.h"
#include <iostream>
#pragma once
using namespace std;

int main()
{
Dictionary dictionary;
dictionary.load();
dictionary.download_words();

In_text word;
word.enter();
if(dictionary.compare(word.get_text()))
     std::cout << "Your word is correct" << std::endl;
else std::cout << "Your word is incorrect" << std::endl;

return 0;
}
